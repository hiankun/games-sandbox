[Thu Oct  3 10:18:15 AM CST 2024]
- try to write a tetris game using curses module.
- status:
    - can spawn random new blocks in random position
    - can only draw the blocks in square pad (cannot handle overlapping)
    - no collision detection
    - no completed line detection (therefore no score counting)
