# https://strategywiki.org/wiki/Tetris/Rotation_systems


class Blocks:
    B = "█"  # U+2588
    b = " "
    I = [
        [b, b, b, b],
        [B, B, B, B],
        [b, b, b, b],
        [b, b, b, b],
    ]
    J = [
        [B, b, b],
        [B, B, B],
        [b, b, b],
    ]
    L = [
        [b, b, B],
        [B, B, B],
        [b, b, b],
    ]
    O = [
        [B, B],
        [B, B],
    ]
    S = [
        [b, B, B],
        [B, B, b],
        [b, b, b],
    ]
    T = [
        [b, B, b],
        [B, B, B],
        [b, b, b],
    ]
    Z = [
        [B, B, b],
        [b, B, B],
        [b, b, b],
    ]
    Empty = [
        [b, b, b],
        [b, b, b],
        [b, b, b],
    ]

    def __init__(self, name=None):
        self.name = name
        match self.name:
            case "I":
                self.block = self.I.copy()
            case "J":
                self.block = self.J.copy()
            case "L":
                self.block = self.L.copy()
            case "O":
                self.block = self.O.copy()
            case "S":
                self.block = self.S.copy()
            case "T":
                self.block = self.T.copy()
            case "Z":
                self.block = self.Z.copy()
            case None:
                self.block = self.Empty.copy()
        self.size = len(self.block)
        self.trans = [[" " for _ in range(self.size)] for _ in range(self.size)]

    def vis(self):
        for r in self.block:
            print(r)

    def reset(self):
        self.__init__(self.name)

    def transpose(self):
        self.trans[:] = list(map(list, zip(*self.block)))

        # s = self.size
        # for i in range(s):
        #    for j in range(s):
        #        self.trans[i][j] = self.block[j][i]

    def rot_p90(self):
        """rotate positive 90 deg
        transpose + reverse rows
        """
        self.transpose()
        self.block[:] = list(reversed(self.trans))

        # s = self.size
        # for i in range(s):
        #    for j in range(s):
        #        self.block[i][j] = self.trans[s - 1 - i][j]

    def rot_m90(self):
        """rotate positive 90 deg
        transpose + reverse columns
        """
        self.transpose()
        self.block[:] = [list(reversed(row)) for row in self.trans]

        # s = self.size
        # for i in range(s):
        #    for j in range(s):
        #        self.block[i][j] = self.trans[i][s - 1 - j]


def main():
    blk_j = Blocks("J")
    blk_j.vis()
    blk_j.rot_m90()
    blk_j.vis()
    blk_j.rot_m90()
    blk_j.vis()


if __name__ == "__main__":
    main()
