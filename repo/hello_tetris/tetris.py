import curses as crs
from curses import wrapper
from blocks import Blocks
import time
import random


class BlkNames:
    name = ("I", "J", "L", "O", "S", "T", "Z")


class BoardSettings:
    pos = (1, 2)
    width = 10 * 2
    height = 20


def draw_board(debug=True):
    bs = BoardSettings()
    board = crs.newpad(bs.height + 2, bs.width + 4)
    #board.border('|', '|', '-', '-','+','+','+','+')
    board.border()

    if debug:
        for y in range(1, bs.height + 1):
            board.addstr(y, 2, "." * bs.width)

    board.refresh(
        0,
        0,
        bs.pos[0],
        bs.pos[1],
        bs.pos[0] + bs.height + 1,
        bs.pos[1] + bs.width + 3,
    )
    return board


def draw_block(blk, y, x, key, scrn):
    if key == crs.KEY_UP:
        blk.rot_m90()

    blk_size = len(blk.block)
    blk_pad = crs.newpad(blk_size + 1, blk_size + 1)
    for r in blk.block:
        blk_pad.addstr("".join(r) + "\n")

    blk_zone = (y, x, y + blk_size - 1, x + blk_size - 1)
    # check_collision(blk.block)

    blk_pad.refresh(0, 0, *blk_zone)


def update_rows(): ...


def touch_down(blk, blk_yx, board, scrn):
    y, x = blk_yx
    h = len(blk.block)
    scrn.addstr(0, 26, "                      ")
    scrn.addstr(0, 26, f"{blk_yx} {h} {board.inch(y+h,x)}")
    # TODO
    if blk_yx[0] > 18:
        # if board.inch(y+2,x) != ord(' '):
        return True
    return False


def check_collision(block):
    transposed = list(map(list, zip(*block)))


def gen_new_block():
    bs = BoardSettings()
    n_idx = random.randint(1, len(BlkNames.name))
    x_loc = random.randint(bs.pos[1] + 1, bs.width + 2)
    block = Blocks(BlkNames.name[n_idx - 1])
    return x_loc, block


def main(scrn):
    scrn.clear()
    crs.curs_set(False)
    scrn.refresh()  # it has to be here, before any pads
    scrn.addstr(0, 8, "Hello Tetris")
    board = draw_board()
    bs = BoardSettings()
    scrn.addstr(bs.pos[0] + bs.height + 2, 0, "Press q to exit")
    scrn.timeout(100)

    dy = 0
    dx = 0
    update_dt = 0.8
    key_prev = None
    time_prev = time.time()

    need_new_block = True

    while True:
        if need_new_block:
            dx, blk = gen_new_block()
            need_new_block = False

        key = scrn.getch()

        if key == ord("q"):
            break
        elif key == crs.KEY_RIGHT:
            dx += 1
        elif key == crs.KEY_LEFT:
            scrn.addstr(0, 26, f"key is {key:04}")
            dx -= 1
        elif key == crs.KEY_UP:
            key_prev = key
        else:
            key = None

        time_now = time.time()
        time_is_up = time_now - time_prev > update_dt
        blk_yx = (bs.pos[0] + dy, dx)
        if time_is_up:
            dy += 1
            board = draw_board()
            blk_yx = (bs.pos[0] + dy, dx)
            draw_block(blk, *blk_yx, key_prev, scrn)
            # reset key and timer
            key_prev = None
            time_prev = time_now

        if key is not None:
            board = draw_board()
            draw_block(blk, *blk_yx, key_prev, scrn)
            # reset key
            key_prev = None

        if touch_down(blk, blk_yx, board, scrn):
            dy = 0
            need_new_block = True


if __name__ == "__main__":
    wrapper(main)
