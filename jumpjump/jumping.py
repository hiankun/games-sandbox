import cv2
import mediapipe as mp
import numpy as np

mp_drawing = mp.solutions.drawing_utils
mp_drawing_styles = mp.solutions.drawing_styles
mp_pose = mp.solutions.pose

head_lm = [
    'NOSE',
    'LEFT_EYE_INNER',
    'LEFT_EYE',
    'LEFT_EYE_OUTER',
    'RIGHT_EYE_INNER',
    'RIGHT_EYE',
    'RIGHT_EYE_OUTER',
    'LEFT_EAR',
    'RIGHT_EAR',
    'MOUTH_LEFT',
    'MOUTH_RIGHT',
]

bg_vid = './data/super_mario.mp4'

input_vids = [
    './data/jumping01.mp4',
    './data/jumping02.mp4',
]

class FrameUpdate:
    def __init__(self):
        self.center_history = [0,0,0]
        self.status_history = [0,0]
        self.counts = -1
        self.center = 0.0
        self.move = -1 #unknown

    def reset(self):
        self.counts = -1

    def update_centers(self):
        _thld = 0.05 #FIX
        self.center_history.pop(0)
        self.center_history.append(self.center)
        center_vec = self.center_history[-1]-self.center_history[0]
        if center_vec > _thld:
            self.move = 1 #upward
        if center_vec < -_thld:
            self.move = 0 #downward
        return center_vec 
    
    def update_status(self):
        self.status_history.pop(0)
        self.status_history.append(self.move)
        a, b = self.status_history
        return a^b
    
    def do_update(self, frame, bg_frame, results):
        if results.pose_landmarks:
            self.center = results.pose_landmarks.landmark[mp_pose.PoseLandmark.NOSE].y
        else:
            self.center = 0.0
        center_vec = self.update_centers()
        if self.move != -1:
            status_changed = self.update_status()
            if status_changed:
                self.counts += 1
    
        # Segmentation
        #frame = cv2.cvtColor(frame, cv2.COLOR_RGB2BGR)
        annotated_image = frame.copy()
        if results.segmentation_mask is not None:
            condition = np.stack((results.segmentation_mask,) * 3, axis=-1) > 0.1
            bg_image = np.zeros(frame.shape, dtype=np.uint8)
            bg_image[:] = bg_frame
            annotated_image = np.where(condition, annotated_image, bg_image)
        else:
            annotated_image = bg_frame
    
        # Draw the pose annotation on the image.
        #mp_drawing.draw_landmarks(
        #    #frame,
        #    annotated_image,
        #    results.pose_landmarks,
        #    mp_pose.POSE_CONNECTIONS,
        #    landmark_drawing_spec=mp_drawing_styles.get_default_pose_landmarks_style()
        #)

        # Flip the image horizontally for a selfie-view display.
        cv2.flip(annotated_image, 1)
        cv2.putText(annotated_image, f'{self.counts//2:>04}', (30,80), 3, 2, (0,255,255), 2)
        return annotated_image


def webcam_test():
    cv2.namedWindow('Jumping!', cv2.WINDOW_NORMAL)
    cap = cv2.VideoCapture(input_vids[1])
    #cap = cv2.VideoCapture('/dev/video0')
    cap_bg = cv2.VideoCapture(bg_vid)
    with mp_pose.Pose(
        model_complexity=1,
        enable_segmentation=True,
        min_detection_confidence=0.5,
        min_tracking_confidence=0.5,
    ) as pose:

        frame_update = FrameUpdate()

        while True:
            success, frame = cap.read()
            bg_ok, bg_frame = cap_bg.read()
            if not bg_ok: #restart the bg video
                cap_bg.release()
                cap_bg = cv2.VideoCapture(bg_vid)
                bg_ok, bg_frame = cap_bg.read()
            h,w,_ = frame.shape
            bg_frame = cv2.resize(bg_frame, (w,h))
            if not success:
                print("Ignoring empty camera frame.")
                continue

            # To improve performance, optionally mark the image as not writeable to
            # pass by reference.
            frame.flags.writeable = False
            #frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
            results = pose.process(frame)
            
            res_img = frame_update.do_update(frame, bg_frame, results)
            cv2.imshow('Jumping!', res_img)

            key = cv2.waitKey(5) & 0xFF
            if key == ord('q'):
                break
            if key == ord('r'):
                frame_update.reset()
    cap.release()
    cap_bg.release()
    cv2.destroyAllWindows()


def main():
    webcam_test()


if __name__=='__main__':
    main()
