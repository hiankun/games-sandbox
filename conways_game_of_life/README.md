* ref:  https://www.conwaylife.com/wiki/Main_Page
# Known issue: 
* Boundary condition is not right, so if cells touched the boundaries the pattern would be messed up.
* Because the logic of testing `the end of evolution', the final generation will exceed the true number by 2.

