import pygame as pg
import sys

pg.init()
screen = pg.display.set_mode((900,600))
pg.display.set_caption('Runner')
clk = pg.time.Clock()
title_txt = pg.font.Font('fonts/Pixeboy-z8XGD.ttf', 50)

spaceship = pg.image.load('graphics/spaceship_set/spaceship02.png') #.convert_alpha()
spaceship_rect = spaceship.get_rect(center=(100,150))
chicken = pg.image.load('graphics/animal_set/chicken.png')
chicken_rect = chicken.get_rect(center=(900,450))

bg = pg.image.load('graphics/background_set/bg.png')
title = title_txt.render('My Game', False, (200,200,0))

spaceship_speed = 10
while True:
    for e in pg.event.get():
        if e.type == pg.QUIT:
            pg.quit()
            sys.exit()
    keys = pg.key.get_pressed()
    if keys[pg.K_LEFT]:
        px = -spaceship_speed
    elif keys[pg.K_RIGHT]:
        px = spaceship_speed
    elif keys[pg.K_UP]:
        py = -spaceship_speed
    elif keys[pg.K_DOWN]:
        py = spaceship_speed
    else:
        px = 0
        py = 0
    screen.blit(bg, (0,-40))
    screen.blit(title, (400,50))

    chicken_rect.left = (chicken_rect.left-5
                           if chicken_rect.left>0 else 900)
    spaceship_rect.move_ip(px,py)

    screen.blit(chicken, chicken_rect)
    screen.blit(spaceship, spaceship_rect)

    pg.display.update()
    clk.tick(15)
